⚠️ **This project has moved to Sourcehut: [~mrshll1001/harvest-cli](https://git.sr.ht/~mrshll1001/harvest-cli). Please raise all issues and submit all patches there.** ⚠️

This repo is left up because there are a few people I know who I've linked to the project and who use it. I recommend you update the git remotes, as I will be taking this repo down eventually

# Harvest CLI

A CLI tool for interacting with [Harvest](https://www.getharvest.com/), written in Python. It does not implement the entire API spec, just some basics.

## Why?

* I prefer using CLI tools where possible because I find them less distracting and easier to use than firing up a web browser
* Also, browsers are quite resource intensive and I don't like chewing up a few GiB of RAM just to log some time on a project!
* The [other Harvest CLI App](https://www.getharvest.com/integrations/hrvst-cli) is written in nodejs and requires `npm`. I don't like installing entire ecosystems just for one app. If you don't mind this, or you already have `npm` installed for other reasons then I guarantee that `hrvst-cli` is more feature-complete and better written.
* I don't write much code any more and wanted to have some fun, and learn how CLIs are built using Python.

## Features

* Basic CRUD for several Harvest objects: Projects, Tasks, Time Entries, and Users
* Choose between plain output (default) or JSON by passing a flag
* Outputs to STDOUT to form part of a pipeline
* Configure local aliases for projects, users, and tasks so you don't need to remember Harverst IDs for regular tasks
* Configuration (aliases and authentication) is done via a local config file in JSON, which respects `$XDG_CONFIG_HOME` and defaults to `~/.config/harvest-cli/config.json`
* Authentication is via getting a Harvest [Personal Access Token](https://id.getharvest.com/sessions/new?go_back=%2Fdevelopers) and pasting it into either the config or using the `config` command to set it up automatically. oAuth2 is not supported and likely won't be in the future, because this CLI tool is mostly designed for personal use and I prefer to use personal access tokens; sorry! I am open to receiving contributions for this though, with the caveat that it must only use dependencies available in the Debian repos (not PyPi!)

## Installation

Installation is via `make`, which will copy the CLI script to `~/.local/bin/harvest-cli` as an executable, and copy the supporting API wrapper class and associated files to `~/.local/lib/python3.11/site-packages/getharvest_api_wrapper`.

* `make install` installs
* `make uninstall` removes the files that were installed

In the future, I am intending to add functionality to the Makefile to support building a `.deb` package. This is so that, if you prefer, you can use `apt` to install/manage it as a package in your system.

### Dependencies

* Python3, and currently assumes 3.11 for installation
* Python libraries:
   * `click`
   * `json`
   * `os`
   * `requests`
   * `sys`

I have deliberately designed it so that I can use libraries which are in the Debian repos rather than relying on PyPi packages. I assume that the script will still work if you install the libraries via `pip` or if you're on another distro.

If you are on Debian or a derivative and want the system package manager to manage the dependencies for you: run the following:

```
sudo apt install python3-click 
```

The other dependencies usually come with Debian's python distribution.

## Contributing

This is pretty much a personal tool which I've designed for my own amusement and use, and to satisfy my own stubbornness around not installing `npm` just to run a single CLI application. I am not expecting any contributions. However, if this has resonated with you and you're keen to contribute then I am open to the following contributions:

* Code cleanup &ndash; My CS degree is very rusty and I was never trained in Python, nor have I endeavoured to learn it well. The code has lots of redunant bits in there, and in general I am open to a tidy up.
* oAuth &ndash; I have no interest in implementing oAuth or using the tool in an oAuth-y manner. However if you feel this would be useful to others/yourself, then I am very open to you implementing the feature as long as the personal access token feature also remains intact from an interactional point of view.
* More Harvest API features &ndash; I have little interest in them, but feel free to go for it.
* Makefile adjustments &ndash; I'm quite new at Makefiles. If there's a better way to write them or even a better way to install the software, please contribute.
