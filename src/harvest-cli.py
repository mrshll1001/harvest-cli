#!/usr/bin/env python3

import os 
import sys
import json
import click


from getharvest_api_wrapper import HarvestAPI
from datetime import date
from datetime import datetime

# --------- Config and global variable stuff --------- #


# If we're not printing to a terminal, or if the user has a NO_COLOR environment variable, we don't print colour. https://no-color.org/

NO_COLOUR = False

# --------- End global variable stuff --------- #

# --------- Quick class for capturing ANSII colour values for pretty printing --------- #

class TermFormat:
    BOLD = '\033[1m'
    BLUE  = "\033[1;34m"
    GREEN = "\033[0;32m"
    YELLOW = '\033[1;33m'
    MAGENTA = '\033[38;5;5m'
    WHITE = '\033[38;5;7m'
    ENDC = '\033[0m'


# --------- End class for ANSII colour values --------- #

# --------- CLI Starts here ----------------

@click.group()
@click.pass_context
@click.option('--plain', 'output_format', flag_value='plain', help="(Default) Set the output format to plain and human-readable. Mutually exclusive with --json", default=True)
@click.option('--json', 'output_format', flag_value='json', help="Set the output format to JSON. Mutually exclusive with --plain")
@click.option('--no-color', 'colour_format', flag_value='no-color', help="No colour in the output. Mutually exclusive with --color. Has no effect when used with --json")
@click.option('--color', 'colour_format', flag_value='color', help="Colorize the output even if not printing to a tty. Mutually exclusive with --no-color. Has no effect when used with --json")
def cli(ctx, output_format, colour_format):
    """
    Harvest CLI Help
    """
    ctx.obj['output_format'] = output_format
    ctx.obj['colour_format'] = colour_format

    global NO_COLOUR

    # Don't print in colour if we're not in a tty, the user has a NO_COLOR environment variable, or has passed --no-color as a flag

    if not sys.stdout.isatty() and colour_format != "color": # We allow for enforcing colour with --color
        NO_COLOUR = True

    if os.environ.get('NO_COLOR', "") != "":
        NO_COLOUR = True

    if colour_format == "no-color":
        NO_COLOUR = True

def get_config_directory():
   """
   get_config_directory returns the location of the configuration directory based on the presence of $XDG_CONFIG_HOME

   :return: os.path representing the config directory
   """

   return os.path.join(os.environ.get('XDG_CONFIG_HOME', os.path.join(os.path.expanduser('~'), '.config')), 'harvest-cli')


def get_config_file_path():
    """
    get_config_file_path returns the location of the configuration file

    :return: os.path representing the config file path
    """

    return os.path.join(get_config_directory(), 'config.json')

def write_configuration_to_disk(config):
    """
    write_configuration_to_disk writes the provided configuration dict to the disk following the XDG Base Directory specification. Default location is ~/.config/harvest-cli/config.json

    :param config: Dict
    """

    # Create configuration directory if it doesn't exist yet

    if not os.path.exists(get_config_directory()):
        os.makedirs(get_config_directory())

    with open(get_config_file_path(), 'w') as config_file:
        config_file.write(json.dumps(config, indent=4))


def get_configuration():
    """
    get_configuration returns the user's configuration file as a dict based on parsed json

    :return: A python dict containing the user configuration as a result of calling json.load on the file
    """
    with open(os.path.join(os.path.expanduser('~'), '.config', 'harvest-cli', 'config.json'), 'r') as configFile:
        return json.load(configFile)

def get_user_agent():
    """
    Returns a user agent string as required by the GetHarvest API

    :return: String representing the user agent
    """

    return "Matt Marshall's Harvest CLI Tool <communications@mrshll.uk>"

def get_harvest_client_from_configuration():
    """
    Returns a HarvestAPI client object by retrieving the configuration

    :return: getharvest_api_wrapper.HarvestAPI
    """
    config = get_configuration()

    return HarvestAPI(get_user_agent(), config['access-token'], config['account-id'])

def resolve_alias(alias, alias_type):
    """
    Returns a Harvest object ID if the alias exists for the given type, or the value of the alias parameter if it doesn't

    :param alias: The alias as a string
    :param alias_type: The type of alias e.g. project, client, task, user, etc

    :return: String. The id if it exists, or the alias parameter.
    """

    try:
        aliases = get_configuration()['aliases'][alias_type]

        if alias in aliases.keys():
            return aliases[alias]

        # If the alias isn't found, then we need to assume that it's an id so we pass it back out
        return alias
    except KeyError:
        # If there's no alias key, then there's no aliases. So return the alias parameter assuming it's an id.
        return alias

def check_date(date_string):
    """
    Returns True if the user has provided a proper datestring in YYYY-MM-DD and throw a ValueError if not

    :param date_string: A string representing the date e.g. '2020-02-02'
    
    :return: Boolean, true if the user has a valid date string
    """

    try:
        datetime.fromisoformat(date_string)
        return True
    except:
        raise ValueError(f"{date_string} is not a valid ISO 8601 date in YYYY-MM-DD") 

def exit_with_error(ctx, error_code, error_message):
    """
    Write some information to STDERR and then exit with a non-zero exit code
    """

    if ctx.obj['output_format'] == 'json':
        sys.stderr.write(json.dumps({"code": error_code, "message": error_message}))
    else:
        sys.stderr.write(f"ERROR: Error {error_code} occurred. {error_message}")

    sys.exit(1)


#==================================
# Alias
#==================================

@cli.group()
@click.pass_context
def alias(ctx):
    """Manage local aliases for Harvest object IDs"""


@alias.command('create', help="Create a new local alias for a Harvest object ID")
@click.pass_context
@click.option('--alias-type', 'alias_type', type=click.Choice(['client', 'project', 'task', 'user'], case_sensitive=False), required=True, prompt=True, help="The alias type. Can be user, client, project, or task")
@click.option('--id', 'harvest_id', required=True, prompt=True, help="The Harvest ID of the object to create the alias for")
@click.option('--alias', 'alias', required=True, prompt=True, help="The local alias of the Harvest Object ID")
def create_alias(ctx, alias_type, harvest_id, alias):

    config = get_configuration()

    if alias in config['aliases'][alias_type]:
        exit_with_error(ctx, "alias-collision", "Alias '%s' already exists." % alias)

    config['aliases'][alias_type].update({alias: harvest_id})

    write_configuration_to_disk(config)

    if ctx.obj['output_format'] == "json":
        print(json.dumps({alias_type: {alias: harvest_id}}))
    else:
        print("SUCCESS:")
        print_alias(alias, harvest_id)


@alias.command('delete', help="Delete a local alias for a Harvest object ID")
@click.pass_context
@click.option('--alias-type', 'alias_type', type=click.Choice(['client', 'project', 'task', 'user'], case_sensitive=False), required=True, prompt=True, help="The alias type. Can be user, client, project, or task")
@click.option('--alias', 'alias', required=True, prompt=True, help="The local alias of the Harvest Object ID")
def delete_alias(ctx, alias_type, alias):

    config = get_configuration()

    try:
        config['aliases'][alias_type].pop(alias)

        write_configuration_to_disk(config)

        if ctx.obj['output_format'] == 'json':
            print(json.dumps({"code": 0, "message": "Removed alias " + alias}))
        else:
            print("SUCCESS: Deleted alias " + alias)

    except KeyError:
        exit_with_error(ctx, "alias-not-found", "Alias '%s' doesn't exist" % alias)

@alias.command("list", help="List the local aliases for Harvest Object IDs")
@click.pass_context
def list_aliases(ctx):
   
    aliases = get_configuration()['aliases']

    if ctx.obj['output_format'] == "json":
        print(json.dumps(aliases))
    else:
        for alias_type in aliases.keys():
            print("Alias type: {}".format(alias_type))
            print("---------------")

            if len(aliases[alias_type].keys()) == 0:
                print("None")
            else:
                for alias in aliases[alias_type].keys():
                    print_alias(alias, aliases[alias_type][alias])
            print("\n")

@alias.command('show', help="Show a local alias for a Harvest object ID")
@click.pass_context
@click.option('--alias-type', 'alias_type', type=click.Choice(['client', 'project', 'task', 'user'], case_sensitive=False), required=True, prompt=True, help="The alias type. Can be user, client, project, or task")
@click.option('--alias', 'alias', required=True, prompt=True, help="The local alias of the Harvest Object ID")
def show_alias(ctx, alias_type, alias):

    config = get_configuration()

    try:
        result = config['aliases'][alias_type][alias]

        if ctx.obj['output_format'] == "json":
            print(json.dumps({alias: result}))
        else:
            print_alias(alias, result)

    except KeyError:
        exit_with_error(ctx, "alias-not-found", "Alias '%s' doesn't exist" % alias)
        
@alias.command('update', help="Update a local alias with a new Harvest object ID")
@click.pass_context
@click.option('--alias-type', 'alias_type', type=click.Choice(['client', 'project', 'task', 'user'], case_sensitive=False), required=True, prompt=True, help="The alias type. Can be user, client, project, or task")
@click.option('--alias', 'alias', required=True, prompt=True, help="The local alias of the Harvest Object ID")
@click.option('--id', 'harvest_id', required=True, prompt=True, help="The Harvest ID of the object to create the alias for")
def update_alias(ctx, alias_type, alias, harvest_id):

    config = get_configuration()

    try:

        config['aliases'][alias_type].update({alias: harvest_id})
        write_configuration_to_disk(config)

        if ctx.obj['output_format'] == "json":
            print(json.dumps({alias: harvest_id}))
        else:
            print("SUCCESS: Updated alias " + alias + " to point to Harvest ID " + harvest_id)

    except KeyError:
        exit_with_error(ctx, "alias-not-found", "Alias '%s' doesn't exist" % alias)


def print_alias(alias, harvest_id):
    """Prints out an alias formatted for --plain"""

    print("Alias: {}\nHarvest ID: {}".format(alias, harvest_id))

#==================================
# Config
#==================================

@cli.group()
@click.pass_context
def config(ctx):
    """Configure this application"""

@config.command('setup', help="Set up this program with your Harvest Account ID and Access Token. Set them up by visiting https://id.getharvest.com/developers")
@click.pass_context
@click.option('--account-id', 'account_id', required=True, prompt=True, help="Your Harvest Account ID")
@click.option('--access-token', 'access_token', required=True, prompt=True, help="Your Harvest Personal Access Token")
@click.option('-f', '--force', is_flag=True, help="Force writing the configuration file even if one already exists. This may result in losing custom configuration such as aliases etc.")
def setup_config(ctx, account_id, access_token, force):

    if os.path.exists(get_config_file_path()) and force == False:

        response = input("A configuration file already exists, are you sure you want to overwrite it? (y/n): ")

        if response != "y":
            sys.exit("ERROR: Did not create configuration file. File already exists. Either re-run this command with the --force option, re-run and type 'y' in the prompt, or remove the existing configuration file")


    config = {'access-token': access_token, 'account-id': account_id, 'aliases': {'client': {}, 'project': {}, 'task': {}, 'user': {}}}

    print("Writing configuration to disk…")

    write_configuration_to_disk(config)

    print("Finished writing configuration to disk")

    print("Retrieving user details from Harvest…")

    user = get_harvest_client_from_configuration().get_current_user().json()

    print(f"You're authenticated as {user['first_name']} {user['last_name']} <{user['email']}> with id {user['id']} ")

    print(f"Creating alias 'me' for user id {user['id']} in configuration file. This avoids needing to make additional API requests for some commands…")

    config['aliases']['user'].update({'me': str(user['id'])})

    write_configuration_to_disk(config)

    print("Done. You're all set up :-) Run harvest-cli --help for a list of commands.")

#==================================
# Projects
#==================================

@cli.group()
@click.pass_context
def project(ctx):
    """Manage Projects"""

@project.command('create', help="Create a Project.")
@click.pass_context
@click.option('--client', required=True, prompt=True, help="The Harvest ID or local alias of the Client associated with the project")
@click.option('--name', required=True, prompt=True, help="The name of the project")
@click.option('--code', help="The code associated with the project")
@click.option('--active/--inactive', 'is_active', default=True, help="Whether the project is active or archived. Defaults to --active")
@click.option('--billable/--non-billable', 'is_billable', required=True, prompt=True, help="Whether the project is billable or not")
@click.option('--fixed-fee', 'is_fixed_fee', is_flag=True, default=False, help="Create the project as fixed fee")
@click.option('--billing-method', 'bill_by', type=click.Choice(['none', 'Project', 'Tasks', 'People'], case_sensitive=False), required=True, prompt=True, help="The method by which the project is invoiced. Can be 'Project', 'Tasks', 'People', or 'none'")
@click.option('--rate', 'hourly_rate', type=float, help="Rate for projects billed by the hourly rate as a decimal")
@click.option('--budget', type=float, help="The budget in hours for the project when budgeting by time. Hours must be a decimal e.g. 1.75 hrs equals 1hr 45mins")
@click.option('--budget-method', 'budget_by', type=click.Choice(['none', 'project', 'project_cost', 'task', 'task_fees', 'person'], case_sensitive=False), required=True, prompt=True, help="The method by which the project is budgeted. Options: project (Hours Per Project), project_cost (Total Project Fees), task (Hours Per Task), task_fees (Fees Per Task), person (Hours Per Person), none (No Budget)")
@click.option('--monthly-budget', 'budget_is_monthly', is_flag=True, default=False, help="Option to have the budget reset monthly")
@click.option('--notify-when-overbudget', 'notify_when_over_budget', is_flag=True, default=False, help="Project Managers should be notified when the project goes over budget")
@click.option('--notify-at', 'over_budget_notification_percentage', type=float, default=None, help="Percentage value used to trigger over budget email alerts. Example: use 10.0 for 10.0%")
@click.option('--reveal-budget', 'show_budget_to_all', is_flag=True, default=False, help="Option to show project budget to all employees. Does not apply to Total Project Fee projects")
@click.option('--cost-budget', 'cost_budget', type=float, default=None, help="The monetary budget for the project when budgeting by money")
@click.option('--cost-includes-expenses', 'cost_budget_include_expenses', is_flag=True, default=False, help="Option for budget of Total Project Fees projects to include tracked expenses")
@click.option('--fee', type=float, default=None, help="The amount you plan to invoice for the project. Only used by fixed-fee projects")
@click.option('--notes', default=None, help="Project notes")
@click.option('--start', 'starts_on', help="Date the project was started in ISO 8601 format YYYY-MM-DD e.g. 2000-01-01")
@click.option('--end', 'ends_on', help="Date the project will end in ISO 8601 format YYYY-MM-DD e.g. 2000-01-01")
def create_project(ctx, client, name, code, is_active, is_billable, is_fixed_fee, bill_by, hourly_rate, budget, budget_by, budget_is_monthly, notify_when_over_budget, over_budget_notification_percentage, show_budget_to_all, cost_budget, cost_budget_include_expenses, fee, notes, starts_on, ends_on):

    if starts_on is not None:
        check_date(starts_on)

    if ends_on is not None:
        check_date(ends_on)

    result = _create_project(ctx, {'client_id': resolve_alias(client, 'client'), 'name': name, 'code': code, 'is_active': is_active, 'is_billable': is_billable, 'is_fixed_fee': is_fixed_fee, 'bill_by': bill_by, 'hourly_rate': hourly_rate, 'budget': budget, 'budget_by': budget_by, 'budget_is_monthly': budget_is_monthly, 'notify_when_over_budget': notify_when_over_budget, 'over_budget_notification_percentage': over_budget_notification_percentage, 'show_budget_to_all': show_budget_to_all, 'cost_budget': cost_budget, 'cost_budget_include_expenses': cost_budget_include_expenses, 'fee': fee, 'notes': notes, 'starts_on': starts_on, 'ends_on': ends_on})

    if ctx.obj['output_format'] == "json":
        print(json.dumps(result))
    else:
        print_project(result)

def _create_project(ctx, params):

    response = get_harvest_client_from_configuration().create_project(params)

    if response.ok:
        return response.json()
    else:
        exit_with_error(ctx, "HTTP %d" % response.status_code, response.text)

@project.command('delete', help="Delete a Project.")
@click.pass_context
@click.option('--project', required=True, prompt=True, help='The Harvest ID or local alias of the Project.')
@click.option('-f', '--force', is_flag=True, help='Force deletion of the Project without prompting for confirmation.')
def delete_project(ctx, project, force):

    if force == False:
        print("WARNING: This action is destructive and might result in the deletion of the Project. Harvest's advice is to archive old projects instead of deleting them. Are you sure you want to do this? You can abort this action with CTRL+C or CTRL+D now.")
        user_input = input("\nPlease retype the Project's alias or id to confirm deletion: ")

        if user_input != project:
            sys.exit("ERROR: Did not delete the Project. Either re-run the command and enter the Project name or alias in the prompt, or re-run the command with the -f/--force flag.")

    result = _delete_project(ctx, resolve_alias(project, 'project'))

    if ctx.obj['output_format'] == 'json':
        print(json.dumps(result))
    else:
        print(f"SUCCESS: Deleted Project {project}") 

def _delete_project(ctx, project_id):

    response = get_harvest_client_from_configuration().delete_project(project_id)

    if response.ok:
        return response.json()
    else:
        exit_with_error(ctx, "HTTP %d" % response.status_code, response.text)


@project.command('list', help="List Projects with associated details.")
@click.pass_context
@click.option('--client', default=None, help="Only return projects belonging associated with the given client id or alias")
@click.option('--updated-since', 'updated_since', default=None, help="Only return projects that have been updated since the given date and time in ISO 8601 format YYYY-MM-DD e.g. 2000-01-01.")
@click.option('--active/--inactive', 'is_active', default=None, help="Only return projects that are active or inactive repectively")
def list_projects(ctx, client, updated_since, is_active):
    """List Projects"""

    if updated_since is not None:
        check_date(updated_since)

    client_id = None
    if client is not None:
        client_id = resolve_alias(client, 'client')

    result = _list_projects(ctx, {'is_active': is_active, 'client_id': client_id, 'updated_since': updated_since })

    if ctx.obj['output_format'] == 'json':
        print(json.dumps(result))
    else:
        first = True
        for p in result['projects']:

            if first: # only print separator above the non-first elements.
                first = False
            else:
                print("\n*****\n")

            print_project(p)

def _list_projects(ctx, params):

    response = get_harvest_client_from_configuration().get_projects(params)

    if response.ok:
        return response.json()
    else:
        exit_with_error(ctx, "HTTP %d" % response.status_code, response.text)


@project.command('show', help="Show details of a Project")
@click.pass_context
@click.option('--project', required=True, prompt=True, help="The Project id or alias")
def show_project(ctx, project):

    result = _show_project(ctx, resolve_alias(project, 'project'))

    if ctx.obj['output_format'] == 'json':
        print(json.dumps(result))
    else:
        print_project(result)

def _show_project(ctx, project_id):

    response = get_harvest_client_from_configuration().get_project_by_id(project_id)

    if response.ok:
        return response.json()
    else:
        exit_with_error(ctx, "HTTP %d" % response.status_code, response.text)

@project.command('update', help="Update a project")
@click.pass_context
@click.option('--project', required=True, prompt=True, help="The Harvest ID or local alias of the project")
@click.option('--client', help="The Harvest ID or local alias of the Client associated with the project")
@click.option('--name', prompt=True, help="The name of the project")
@click.option('--code', help="The code associated with the project")
@click.option('--active/--inactive', 'is_active', default=None, help="Whether the project is active or archived")
@click.option('--billable/--non-billable', 'is_billable', help="Whether the project is billable or not")
@click.option('--fixed-fee', 'is_fixed_fee', is_flag=True, help="Create the project as fixed fee")
@click.option('--billing-method', 'bill_by', type=click.Choice(['none', 'Project', 'Tasks', 'People'], case_sensitive=False), help="The method by which the project is invoiced. Can be 'Project', 'Tasks', 'People', or 'none'")
@click.option('--rate', 'hourly_rate', type=float, help="Rate for projects billed by the hourly rate as a decimal")
@click.option('--budget', type=float, help="The budget in hours for the project when budgeting by time. Hours must be a decimal e.g. 1.75 hrs equals 1hr 45mins")
@click.option('--budget-method', 'budget_by', type=click.Choice(['none', 'project', 'project_cost', 'task', 'task_fees', 'person'], case_sensitive=False), help="The method by which the project is budgeted. Options: project (Hours Per Project), project_cost (Total Project Fees), task (Hours Per Task), task_fees (Fees Per Task), person (Hours Per Person), none (No Budget)")
@click.option('--monthly-budget', 'budget_is_monthly', is_flag=True, help="Option to have the budget reset monthly")
@click.option('--notify-when-overbudget', 'notify_when_over_budget', is_flag=True, help="Project Managers should be notified when the project goes over budget")
@click.option('--notify-at', 'over_budget_notification_percentage', type=float, default=None, help="Percentage value used to trigger over budget email alerts. Example: use 10.0 for 10.0%")
@click.option('--reveal-budget', 'show_budget_to_all', is_flag=True, default=False, help="Option to show project budget to all employees. Does not apply to Total Project Fee projects")
@click.option('--cost-budget', 'cost_budget', type=float, default=None, help="The monetary budget for the project when budgeting by money")
@click.option('--cost-includes-expenses', 'cost_budget_include_expenses', is_flag=True, default=False, help="Option for budget of Total Project Fees projects to include tracked expenses")
@click.option('--fee', type=float, default=None, help="The amount you plan to invoice for the project. Only used by fixed-fee projects")
@click.option('--notes', default=None, help="Project notes")
@click.option('--start', 'starts_on', help="Date the project was started in ISO 8601 format YYYY-MM-DD e.g. 2000-01-01")
@click.option('--end', 'ends_on', help="Date the project will end in ISO 8601 format YYYY-MM-DD e.g. 2000-01-01")
def update_project(ctx, project, client, name, code, is_active, is_billable, is_fixed_fee, bill_by, hourly_rate, budget, budget_by, budget_is_monthly, notify_when_over_budget, over_budget_notification_percentage, show_budget_to_all, cost_budget, cost_budget_include_expenses, fee, notes, starts_on, ends_on):

    if starts_on is not None:
        check_date(starts_on)

    if ends_on is not None:
        check_date(ends_on)

    client_id = None
    if client is not None:
        client_id = resolve_alias(client, 'client')

    result = _update_project(ctx, resolve_alias(project, 'project'), {'client_id': client_id, 'name': name, 'code': code, 'is_active': is_active, 'is_billable': is_billable, 'is_fixed_fee': is_fixed_fee, 'bill_by': bill_by, 'hourly_rate': hourly_rate, 'budget': budget, 'budget_by': budget_by, 'budget_is_monthly': budget_is_monthly, 'notify_when_over_budget': notify_when_over_budget, 'over_budget_notification_percentage': over_budget_notification_percentage, 'show_budget_to_all': show_budget_to_all, 'cost_budget': cost_budget, 'cost_budget_include_expenses': cost_budget_include_expenses, 'fee': fee, 'notes': notes, 'starts_on': starts_on, 'ends_on': ends_on })

    if ctx.obj['output_format'] == 'json':
        print(json.dumps(result))
    else:
        print("SUCCESS: Updated Project")
        print_project(result)

def _update_project(ctx, project_id, params):

    response = get_harvest_client_from_configuration().update_project(project_id, params)

    if response.ok:
        return response.json()
    else:
        exit_with_error(ctx, "HTTP %d" % response.status_code, response.text)



def print_project(project):
    """Prints a Harvest Project for --plain"""

    print("Project: {name} ({code})".format(name = project['name'], code = project['code']))
    print("Harvest ID: {project_id}".format(project_id = project['id']))
    print("Client: {client_name} (ID: {client_id})".format(client_name = project['client']['name'], client_id = project['client']['id']))

    print("")
    print("Starts: {start_date}".format(start_date = project['starts_on']))
    print("Ends: {end_date}".format(end_date = project['ends_on']))

    print("")
    print("Billable: {is_billable}".format(is_billable = project['is_billable']))
    print("Billing Method: {bill_by}".format(bill_by = project['bill_by']))
    if project['is_billable'] == True:
        if project['hourly_rate'] is not None:
            print("Hourly Rate: {rate} {currency}".format(rate = project['hourly_rate'], currency = project['client']['currency']))
    if project['fee'] is not None:
        print("Fixed Fee: {fee}".format(fee = project['fee']))

    print("")
    print("Budget Method: {budget_by}".format(budget_by = project['budget_by']))
    if project['budget'] is not None:
        print("Budget (hours): {budget}".format(budget = project['budget']))
    if project['cost_budget'] is not None:
        print("Budget (costs): {cost_budget}".format(cost_budget = project['cost_budget']))
    print("Total Fees include expenses: {include_expenses}".format(include_expenses = project['cost_budget_include_expenses']))
    print("Budget is Monthly: {monthly}".format(monthly = project['budget_is_monthly']))


#==================================
# TODO: Project User Assignments and Project Task assignments
#==================================

#==================================
# Tasks
#==================================

@cli.group()
@click.pass_context
def task(ctx):
    """Manage Tasks"""


@task.command('create')
@click.pass_context
@click.option('--name', prompt=True, help="The name of the task")
@click.option('--active/--inactive', 'is_active', default=True, help="Whether the task is active or archived. Defaults to --active")
@click.option('--default', 'is_default', default=False, help="Whether the task is added by default to new projects. Defaults to not.")
@click.option('--billable', 'billable_by_default', default=True, help="Whether the task is billable by default. Defaults to true")
@click.option('--hourly-rate', 'default_hourly_rate', type=float, default=0, help="The default hourly rate for this task, defaults to 0")
def create_task(ctx, name, is_active, is_default, billable_by_default):
    """Create a Task"""
    
    result = _create_task(ctx, {"name": name, "is_default": is_default, "billable_by_default": billable_default, "is_active": is_active, 'default_hourly_rate': default_hourly_rate})

    if ctx.obj['output_format'] == "json":
        print(json.dumps(result))
    else:
        print_task(result)

def _create_task(ctx, params):
    response = get_harvest_client_from_configuration().create_task(params)

    if response.ok:
        return response.json()
    else:
        exit_with_error(ctx, "HTTP %d" % response.status_code, response.text)

@task.command('delete')
@click.pass_context
@click.option('--task', required=True, prompt=True, help="The Harvest ID or local alias of the task")
@click.option('-f', '--force', is_flag=True, help="Force deletion of the Task without prompting for confirmation")
def delete_task(ctx, task, force):
    """Delete Task"""

    if force == False:
        print("WARNING: This action is destructive and might result in the deletion of the Task. Harvest's advice is to archive old Tasks instead of deleting them. Are you sure you want to do this? You can abort this action with CTRL+C or CTRL+D now.")

        user_input = input("\nPlease retype the Project's alias or id to confirm deletion: ")

        if user_input != task:
            sys.exit("ERROR: Did not delete the Task. Either re-run the command and enter the Task name or alias in the prompt, or re-run the command with the -f/--force flag.")

    result = _delete_task(ctx, resolve_alias(task, 'task'))

    if ctx.obj['output_format'] == 'json':
        print(json.dumps(result))
    else:
        print(f"SUCCESS: Deleted Task {Task}")

def _delete_task(ctx, task_id):

    response = get_harvest_client_from_configuration().delete_task(params)

    if response.ok:
        return response.json()
    else:
        exit_with_error(ctx, "HTTP %d" % response.status_code, response.text)

@task.command('list')
@click.pass_context
@click.option('--active/--inactive', 'is_active', default=None, help="Only return tasks that are active or inactive repectively")
@click.option('--updated-since', 'updated_since', default=None, help="Only return tasks that have been updated since the given date and time in ISO 8601 format YYYY-MM-DD e.g. 2000-01-01.")
def list_tasks(ctx, is_active, updated_since):
    """List Tasks"""

    if updated_since is not None:
        check_date(updated_since)

    result = _list_tasks(ctx, {'is_active': is_active, 'updated_since': updated_since})

    if ctx.obj['output_format'] == 'json':
        print(json.dumps(result))
    else:
        first = True
        for t in result['tasks']:

            if first: # only print separator above the non-first elements.
                first = False
            else:
                print("\n*****\n")

            print_task(t)

def _list_tasks(ctx, params):
    
    response = get_harvest_client_from_configuration().get_tasks(params)

    if response.ok:
        return response.json()
    else:
        exit_with_error(ctx, "HTTP %d" % response.status_code, response.text)

@task.command('show')
@click.pass_context
@click.option('--task', required=True, prompt=True, help="The Harvest ID or local alias of the task")
def show_task(ctx, task):
    """Show Task"""

    result = _show_task(ctx, resolve_alias(task, 'task'))

    if ctx.obj['output_format'] == 'json':
        print(json.dumps(result))
    else:
        print_task(result)

def _show_task(ctx, task_id):
    response = get_harvest_client_from_configuration().get_task_by_id(task_id)

    if response.ok:
        return response.json()
    else:
        exit_with_error(ctx, "HTTP %d" % response.status_code, response.text)

@task.command('update')
@click.pass_context
@click.option('--task', required=True, prompt=True, help="The Harvest ID or local alias of the task")
@click.option('--name', prompt=True, help="The name of the task")
@click.option('--active/--inactive', 'is_active', default=True, help="Whether the task is active or archived. Defaults to --active")
@click.option('--default', 'is_default', default=False, help="Whether the task is added by default to new projects. Defaults to not.")
@click.option('--billable', 'billable_by_default', default=True, help="Whether the task is billable by default. Defaults to true")
@click.option('--hourly-rate', 'default_hourly_rate', type=float, default=0, help="The default hourly rate for this task, defaults to 0")
def update_task(ctx, task, is_active, is_default, billable_by_default, default_hourly_rate):
    """Update Task"""

    result = _update_task(ctx, resolve_alias(task, 'task'), {'is_active': is_active, 'is_default': is_default, 'billable_default': billable_default, 'default_hourly_rate': default_hourly_rate})

def _update_task(ctx, task_id, params):

    response = get_harvest_client_from_configuration().update_task(task_id, params)

    if response.ok:
        return response.json()
    else:
        exit_with_error(ctx, "HTTP %d" % response.status_code, response.text)


def print_task(task):
    """Prints a Harvest Task for --plain"""

    print("Task: {name}".format(name = task['name']))
    print("Harvest ID: {hId}".format(hId = task['id']))
    print("")
    print("Is active: {is_active}".format(is_active = task['is_active']))
    print("Is default: {is_default}".format(is_default = task['is_default']))
    print("")
    print("Billable by default: {billable_default}".format(billable_default = task['billable_by_default']))
    print("Default hourly rate: {hourly_rate}".format(hourly_rate = task['default_hourly_rate']))
    print("")
    print("Created: {created}".format(created = task['created_at']))
    print("Updated: {updated}".format(updated = task['updated_at']))

#==================================
# Time Entries
#==================================
@cli.group()
@click.pass_context
def time(ctx):
    """Manage Time Entries"""

@time.command('create')
@click.pass_context
@click.option('--user', 'user_id', default=None, help="The ID of the user to associate with the time entry. Defaults to the currently authenticated user’s ID.")
@click.option('--project', 'project_id', required=True, prompt=True, help="The Harvest ID or local alias of the project to associate with the time entry.")
@click.option('--task', 'task_id', required=True, prompt=True, help="The Harvest ID or local alias of the task to associate with the time entry.")
@click.option('--date', 'spent_date', help="The ISO 8601 formatted date the time entry was spent e.g. 2000-01-01. Will default to TODAY if not provided.")
@click.option('--hours', 'hours', type=float, default=None, help="The current amount of time tracked. If provided, the time entry will be created with the specified hours and is_running will be set to false. If not provided, hours will be set to 0.0 and is_running will be set to true.")
@click.option('--notes', 'notes', default=None, help="Any notes to be associated with the time entry.")
def create_time_entry(ctx, user_id, project_id, task_id, spent_date, hours, notes):
    """Create a Time Entry"""

    if spent_date is None:
        spent_date = datetime.now().strftime("%Y-%m-%d")

    else:
        check_date(spent_date)

    if user_id is not None:
        user_id = resolve_alias(user_id, 'user')

    project_id = resolve_alias(project_id, 'project')
    task_id = resolve_alias(task_id, 'task')

    result = _create_time_entry(ctx, {'user_id': user_id, 'project_id': project_id, 'task_id': task_id, 'spent_date': spent_date, 'hours': hours, 'notes': notes})

    if ctx.obj['output_format'] == "json":
        print(json.dumps(result))
    else:
        print_time_entry(result)


def _create_time_entry(ctx, params):

    response = get_harvest_client_from_configuration().create_time_entry(params)

    if response.ok:
        return response.json()
    else:
        exit_with_error(ctx, "HTTP %d" % response.status_code, response.text)


@time.command('delete')
@click.pass_context
@click.option('--id', 'time_entry_id', required=True, prompt=True, help="The Harvest ID of the Time Entry")
@click.option('-f', '--force', is_flag=True, help="Force deletion of the Task without prompting for confirmation")
def delete_time_entry(ctx, time_entry_id, force):
    """Delete Time Entry"""

    if force == False:
        print("WARNING: This action is destructive and might result in the deletion of the Time Entry. Deleting a time entry is only possible if it’s not closed and the associated project and task haven’t been archived. However, Admins can delete closed entries.")

        user_input = input("\nPlease retype the Project's alias or id to confirm deletion: ")

        if user_input != time_entry_id:
            sys.exit("ERROR: Did not delete the Time Entry. Either re-run the command and enter the Time Entry ID in the prompt, or re-run the command with the -f/--force flag.")


        result = _delete_time_entry(ctx, time_entry_id)

        if ctx.obj['output_format'] == 'json':
            print(json.dumps(result))
        else:
            print(f"SUCCESS: Deleted Time Entry {time_entry_id}")


def _delete_time_entry(ctx, time_entry_id):

    response = get_harvest_client_from_configuration().delete_time_entry(time_entry_id)

    if response.ok:
        return response.json()
    else:
        exit_with_error(ctx, "HTTP %d" % response.status_code, response.text)


@time.command('list')
@click.pass_context
@click.option('--user', 'user_id', default=None, help="Only return Time Entries associated with the given user id or alias")
@click.option('--client', 'client_id', default=None, help="Only return Time Entries associated with the given client id or alias")
@click.option('--project', 'project_id', default=None, help="Only return Time Entries associated with the given project id or alias")
@click.option('--task', 'task_id',default=None, help="Only return Time Entries associated with the given task id or alias")
@click.option('--billed/--unbilled', 'is_billed', default=None, help="Only return time entries that have been billed or are unbilled respectively")
@click.option('--running/--stopped', 'is_running', default=None, help="Only return time entries that are running or have been stopped respectively")
@click.option('--updated-since', 'updated_since', default=None, help="Only return time entries that have been updated since the given date and time in ISO 8601 format YYYY-MM-DD e.g. 2000-01-01.")
@click.option('--from', 'date_from', default=None, help="Only return time entries with a spent date on or after the given date and time date in ISO 8601 format YYYY-MM-DD e.g. 2000-01-01.")
@click.option('--to', 'date_to', default=None, help="Only return time entries with a spent date on or before the given date and time date in ISO 8601 format YYYY-MM-DD e.g. 2000-01-01.")
def list_time_entries(ctx, user_id, client_id, project_id, task_id, is_billed, is_running, updated_since, date_from, date_to):
    """List Time Entries"""

    if updated_since is not None:
        check_date(updated_since)

    if date_from is not None:
        check_date(date_from)

    if date_to is not None:
        check_date(date_to)

    if user_id is not None:
        user_id = resolve_alias(user_id, 'user')

    if client_id is not None:
        client_id = resolve_alias(client_id, 'client')
    
    if project_id is not None:
        project_id = resolve_alias(project_id, 'project')

    if user_id is not None:
        user_id = resolve_alias(user_id, 'user')

    result = _list_time_entries(ctx, {'user_id': user_id, 'client_id': client_id, 'project_id': project_id, 'task_id': task_id, 'is_billed': is_billed, 'is_running': is_running, 'updated_since': updated_since, 'from': date_from, 'to': date_to })

    if ctx.obj['output_format'] == 'json':
        print(json.dumps(result))
    else:
        first = True
        for t in result['time_entries']:

            if first: # only print seperator above non-first elements
                first = False
            else:
                print ("\n*****\n")

            print_time_entry(t)

def _list_time_entries(ctx, params):

    response = get_harvest_client_from_configuration().get_time_entries(params)

    if response.ok:
        return response.json()
    else:
        exit_with_error(ctx, "HTTP %d" % response.status_code, response.text)



@time.command('restart', help="Restart a stopped Time Entry")
@click.pass_context
@click.option('--id', 'time_entry_id', required=True, prompt=True, help="The Time Entry id")
def restart_time_entry(ctx, time_entry_id):

    result = _restart_time_entry(ctx, time_entry_id)

    if ctx.obj['output_format'] == 'json':
        print(json.dumps(result))
    else:
        print_time_entry(result)
def _restart_time_entry(ctx, time_entry_id):

    response = get_harvest_client_from_configuration().restart_stopped_time_entry(time_entry_id)

    if response.ok:
        return response.json()
    else:
        exit_with_error(ctx, "HTTP %d" % response.status_code, response.text)



@time.command('show', help="Show details of a Time Entry")
@click.pass_context
@click.option('--id', 'time_entry_id', required=True, prompt=True, help="The Time Entry id")
def show_time_entry(ctx, time_entry_id):

    result = _show_time_entry(ctx, time_entry_id)

    if ctx.obj['output_format'] == 'json':
        print(json.dumps(result))
    else:
        print_time_entry(result)


def _show_time_entry(ctx, time_entry_id):
    response = get_harvest_client_from_configuration().get_time_entry_by_id(time_entry_id)

    if response.ok:
        return response.json()
    else:
        exit_with_error(ctx, "HTTP %d" % response.status_code, response.text)

@time.command('stop', help="Stop a running Time Entry")
@click.pass_context
@click.option('--id', 'time_entry_id', required=True, prompt=True, help="The Time Entry id")
def stop_time_entry(ctx, time_entry_id):

    result = _stop_time_entry(ctx, time_entry_id)

    if ctx.obj['output_format'] == 'json':
        print(json.dumps(result))
    else:
        print_time_entry(result)


def _stop_time_entry(ctx, time_entry_id):
    response = get_harvest_client_from_configuration().stop_running_time_entry(time_entry_id)

    if response.ok:
        return response.json()
    else:
        exit_with_error(ctx, "HTTP %d" % response.status_code, response.text)

@time.command('update', help="Update a Time Entry")
@click.option('--id', 'time_entry_id', required=True, prompt=True, help="The Time Entry id")
@click.option('--project', 'project_id', help="The Harvest ID or local alias of the project to associate with the time entry.")
@click.option('--task', 'task_id', help="The Harvest ID or local alias of the task to associate with the time entry.")
@click.option('--date', 'spent_date', help="The ISO 8601 formatted date the time entry was spent e.g. 2000-01-01.")
@click.option('--hours', 'hours', type=float, default=None, help="The current amount of time tracked. If provided, the time entry will be created with the specified hours and is_running will be set to false. If not provided, hours will be set to 0.0 and is_running will be set to true.")
@click.option('--notes', 'notes', default=None, help="Any notes to be associated with the time entry.")
def update_time_entry(ctx, time_entry_id, project_id, task_id, spent_date, hours, notes):

    if spent_date is not None: 
        check_date(spent_date)

    if project_id is not None:
        project_id = resolve_alias(project_id, 'project')

    if task_id is not None:
        task_id = resolve_alias(task_id, 'task')

    result = _update_time_entry(ctx, )

    if response.ok:
        return response.json()
    else:
        exit_with_error(ctx, "HTTP %d" % response.status_code, response.text)

def _update_time_entry(ctx, time_entry_id, params):
    
    response = get_harvest_client_from_configuration().update_time_entry(project_id, params)

    if response.ok:
        return response.json()
    else:
        exit_with_error(ctx, "HTTP %d" % response.status_code, response.text)



def print_time_entry(time_entry):
    """Prints a Harvest Time Entry for --plain"""

    if NO_COLOUR:
        print(f"{time_entry['task']['name']} • {time_entry['project']['name']} • {time_entry['client']['name']}")

        print(f"{time_entry['hours']} hours • {time_entry['user']['name']} ({time_entry['user']['id']}) added on {time_entry['spent_date']}")

        print("")

        if time_entry['notes'] is not None:
            print(f"{time_entry['notes']}")

    else:
        print(f"{TermFormat.BOLD}{time_entry['task']['name']}{TermFormat.ENDC} {TermFormat.MAGENTA}{time_entry['project']['name']}{TermFormat.ENDC} • {time_entry['client']['name']}")

        print(f"{TermFormat.GREEN}{time_entry['hours']} hours {TermFormat.ENDC}• {time_entry['user']['name']} ({time_entry['user']['id']}) added on {time_entry['spent_date']}")

        print("")

        if time_entry['notes'] is not None:
            print(f"{TermFormat.WHITE}{time_entry['notes']}{TermFormat.ENDC}")


#==================================
# Users
#==================================

@cli.group()
@click.pass_context
def user(ctx):
    """Manage Users"""



# Entry to the program
if __name__ == "__main__":
    cli(obj={})

