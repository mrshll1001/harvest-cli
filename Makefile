.PHONY: default install uninstall

default: install

install: ./src/harvest-cli.py ./src/getharvest_api_wrapper/harvest_api.py ./src/getharvest_api_wrapper/__init__.py
	mkdir -p $$HOME/.local/lib/python3.11/site-packages
	cp -rv ./src/getharvest_api_wrapper $$HOME/.local/lib/python3.11/site-packages
	cp -v ./src/harvest-cli.py $$HOME/.local/bin/harvest-cli

uninstall:
	rm -rfv $$HOME/.local/lib/python3.11/site-packages/getharvest_api_wrapper
	rm -rv $$HOME/.local/bin/harvest-cli
